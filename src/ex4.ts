import readline from 'readline-sync';

let b:number = readline.questionInt('Digite um valor para b(base): ');
let e:number = readline.questionInt('Digite um valor para e(expoente): ');

let pow = (x: number, y: number): number => {
    let result = 1;
    if ( e == 0 ){
        return 1;
    } else {
        for (let i = 0; i < e; i++){
            result *= b;
    }
}
return result;
};
console.log('pow:' + pow(b, e));


console.log('Versão recursiva');
let b2:number = readline.questionInt('Digite um valor para b(base): ');
let e2:number = readline.questionInt('Digite um valor para e(expoente): ');

let recursivePow = (b2: number, e2: number): number => {
    if (e2 === 0) {
        return 1;
    }else {
        return b2 * recursivePow(b2, e2 -1 );
    }
};
console.log('recursivePow: ' + recursivePow(b2,e2));