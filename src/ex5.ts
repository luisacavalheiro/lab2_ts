import { question } from "readline-sync";

let input: string = question("Digite uma palavra: ");

let toMaiusculaPrimeira = (input: string): string => {
    return input[0].toUpperCase() + input.slice(1);
};
console.log("toMaiusculaPrimeira: " + toMaiusculaPrimeira(input));
